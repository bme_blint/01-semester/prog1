#include <stdio.h>
#include <stdlib.h>

int main()
{
    int szam;

    printf("Kerek egy atalakitando szamot! ");
    scanf("%d", &szam);
    printf("\n");

    if (szam < 100)
    {
        if (szam >= 90)
        {
            printf("XC");
            szam -= 90;
        }

        if (szam >= 50)
        {
            printf("L");
            szam -= 50;
        }

        if (szam >= 40)
        {
            printf("XL");
            szam -= 40;
        }

        while (szam >= 10)
        {
            printf("X");
            szam -= 10;
        }

        if (szam >= 9)
        {
            printf("IX");
            szam -= 9;
        }

        if (szam >= 5)
        {
            printf("V");
            szam -= 5;
        }

        if (szam >= 4)
        {
            printf("IV");
            szam -= 4;
        }

        while (szam >= 1)
        {
            printf("I");
            szam -= 1;
        }

    }
    else
    {
        printf("Ezt nem tudom atirni!");
    }

    printf("\n");

    return 0;
}
