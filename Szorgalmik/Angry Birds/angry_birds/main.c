#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double g = 9.81;

typedef struct coord{
    double x, y;
    } co;

co rt_calc(co r0, double v0, double phi, double t)
{
    co rt;
    rt.x = v0 * cos(phi);
    rt.y = v0 * sin(phi) *t - 0.5 * g * t*t;

    return rt;
}

void calc(co bird, co pig)
{
    double dist = pig.x - bird.x;
    co moment;

    int t = 0;
    while (moment != pig || moment.y != 0)
    {
        for (int v0 = 10; v0 <= 30; v0++)
        {
            for (int phi = 0; phi <= 45; phi+=5)
            {
                moment = rt_calc(bird, v0, phi, t);
                printf("v0 = %d, phi = %d t = %d\n", vo, phi, t);
            }
        }
        t++;
    }

}


void field(char *filename, double bird_high, double pig_high, double distance)
{
    double rbird = 10.0, rpig = 10.0;


    FILE *fp;
    fp = fopen(filename, "w+");
    fprintf(fp, "<svg width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg""\" version=\"1.1\"> \n"); //svg fajl elso sora



    fprintf(fp, "</svg>"); //svg fajl utolso sora
    fclose(fp);
}

int main()
{
    co bird, pig;
    bird.x = 0.0;
    bird.y = 3.0;
    pig.x = 30.0;
    pig.y = 7.0;

    calc(bird, pig);

    return 0;
}
