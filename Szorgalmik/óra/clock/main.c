#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int H = 800, W = 800; //rajzvaszon szelessege
double radius;
double PI = 3.14159;

void setRad() //a fuggveny segitsegevel vizsgalom, hogy melyik oldal nagyobb es beallitom a radiust ennek az oldalnak a 90%-ara, hogy biztos ne logjon tul a kep
{
    if (W < H)
	{
		radius = 0.90 * (W / 2);
	}
	else
	{
		radius = 0.90 * (H / 2);
	}
}

void mkLine(FILE *fp, double start_coord, double end_coord, double angle, double str_w)
{
    double startX = 0.0;
    double startY = 0.0;
    double endX = 0.0;
    double endY = 0.0;

     //printf("startX: %f,\nstartY: %f,\nendX: %f,\nendY %f\n", startX, startY, endX, endY);

    startX = W / 2 + (cos(angle) * start_coord); //kiszamolom a vonal koordinatait
    startY = W / 2 - (sin(angle) * start_coord);
      endX = W / 2 + (cos(angle) *   end_coord);
      endY = W / 2 - (sin(angle) *   end_coord);

    fprintf(fp, "<line x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" stroke=\"orange\" stroke-width=\"%f\" />\n", startX, startY, endX, endY, str_w);

}

void mkSomeLine(FILE *fp, int how_much, double length, int str_w) //how_much hany vonalatat akarok kirajzolni, length milyen hosszuak legyenek a vonalak, str_w vonal vastagsag
{
    double r1, r2; //belso es kulso sugar
    double alpha = 0.0;
    double betha = 0.0;

    betha = 2 * PI / how_much;
    alpha = betha;

    r2 = radius;
    r1 = r2 - length;

    for (int i = 0; i < how_much; i++)
    {
        mkLine(fp, r1, r2, alpha, str_w);
        alpha += betha;
    }
}

void clockPlate(FILE *fp)
{

    setRad();
    fprintf(fp, "<circle cx=\"%d\" cy=\"%d\" r=\"%f\" stroke=\"orange\" stroke-width=\"%d\" fill=\"black\" />\n", W / 2, H / 2, radius, 15);

    mkSomeLine(fp, 60, radius / 12,  1);
    mkSomeLine(fp, 12, radius / 10, 1);
    mkSomeLine(fp,  4, radius /  8, 1);


}

void clock(char *filename)
{
    int h, m, s;
    double alpha;
    double r1 = 0.0;

    printf("Hany az ora, Vekker ur? ");
    scanf("%d %d %d", &h, &m, &s);


    FILE *fp;
    fp = fopen(filename, "w+");
    fprintf(fp, "<svg width=\"%d\" height=\"%d\" xmlns=\"http://www.w3.org/2000/svg""\" version=\"1.1\"> \n", W, H); //svg fajl elso sora

    clockPlate(fp);

    alpha = (- PI / 30) * (s - 15);
    mkLine(fp, r1, radius * 0.7, alpha, 1);

    alpha = (-PI / 30) * (m - 15);
	mkLine(fp, r1, radius * 0.8, alpha, 5);

	alpha = ( - PI / 30) * ((h * 5) - 15) + (- PI / 6) / 60 * m;
	mkLine(fp,r1, radius * 0.5, alpha, 5);


    fprintf(fp, "<circle cx=\"%d\" cy=\"%d\" r=\"%f\" stroke=\"orange\" stroke-width=\"%d\" fill=\"orange\" />\n", W / 2, H / 2, radius * 0.015, 0);




    fprintf(fp, "</svg>"); //svg fajl utolso sora
    fclose(fp);
}


int main(void)
{
    clock("ora.svg");
    return 0;
}
