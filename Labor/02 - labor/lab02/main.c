#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void feladat5()
{
    printf("A printf(\"hello\\n\") kiirja, hogy \"hello\",\nes "
           "kezd egy uj sort.\n\na %%d segitsegevel egy valtozo erteket tudjuk kiirni.\n");
}

void feladat6()
{
    double h, d, r;
    double PI = 3.14159;
    double kerulet, terulet;
    double felulet = 0.0;
    double festek;

    printf("Tartaly festese\n\nMilyen magas? ");
    scanf("%lf", &h);
    printf("\nMennyi az atmeroje? ");
    scanf("%lf", &d);

    r = d / 2;

    kerulet = 2 * r * PI;
    terulet = r * r * PI;

    felulet = terulet * 2 + kerulet * h;

    festek = felulet / 2;

    printf("\n\n%f doboz festek kell.\n", festek);

}

void feladat7()
{
    double a, b, c;
    double x1, x2;
    double gyokalatt;

    printf("Add meg az egyenlet egyutthatoit!\na: ");
    scanf("%lf", &a);
    printf("b: ");
    scanf("%lf", &b);
    printf("c: ");
    scanf("%lf", &c);
    printf("\n");

    gyokalatt = b*b - 4*a*c;

    if (gyokalatt < 0)
    {
        printf("Ennek az egyenletnek nincs valos gyoke!");
    }
    else if (gyokalatt == 0)
    {
        x1 =( -b + sqrt(gyokalatt)) / (2*a);
        printf("Az egyenletnek csak egy valos gyoke van: %f\n", x1);
    }
    else
    {
        x1 =( -b + sqrt(gyokalatt)) / (2*a);
        x2 =( -b - sqrt(gyokalatt)) / (2*a);

        printf("Az egyenlet elso valos megoldasa x1: %f\nmasodik megoldasa x2: %f\n", x1, x2);
    }
}

void feladat8a()
{
    int szam = 1;

    while (szam <= 20)
    {
        printf("%3d\n", szam);
        szam++;
    }
}

void feladat8b()
{

    for (int szam = 1; szam <= 20; szam++)
    {
        printf("%3d\n", szam);
    }
}

void feladat9()
{
    //letrehozok egy valtozot (mekkora)
    int mekkora;

    //megkerdezem, mekkora legyen
    printf("Mekkora legyen a szakasz? ");
    //beirom az adatot a mekkora nevu valtozomba
    scanf("%d", &mekkora);

    //kiirok egy +-t
    printf("+");

    //kiirom mekkora-szor a - jelet
    for (int i = 0; i < mekkora; i++)
    {
        printf("-");
    }

    //kiirom a +-t es egy uj sort kezdek
    printf("+\n");
}

int main()
{
    //feladat5();
    //feladat6();
    //feladat7();
    //feladat8a();
    //feladat8b();
    feladat9();
    return 0;
}
