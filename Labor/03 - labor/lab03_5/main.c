#include <stdio.h>
#include <stdlib.h>

int mytomb[10] = {25, 69, 54, 8, 77, 6, 29, 10, 3, 98}; //mert lusta vagyok sajat szamokat kitalalni

typedef struct tomb {
    int ertek, index;
} tomb;

tomb legkisebb()
{
    tomb lk_szam;
    lk_szam.ertek = mytomb[0];

    for (int i = 0; i < 10; i++)
    {
        if (lk_szam.ertek > mytomb[i])
        {
            lk_szam.ertek = mytomb[i];
            lk_szam.index = i;
        }
    }

    return lk_szam;

}

int main()
{
    tomb legK_szam;
    legK_szam = legkisebb();

    printf("A tomb: ");
    for (int i = 0; i < 10; i++)
    {
        if (i == legK_szam.index)
        {
            printf("%d[MIN] ", mytomb[i]);
        }
        else
        {
            printf("%3d ", mytomb[i]);
        }
    }

    printf("\nA legkisebb szam: %d\n", legK_szam.ertek);
    printf("A legkisebb szam indexe: %d\n", legK_szam.index);
    return 0;
}
