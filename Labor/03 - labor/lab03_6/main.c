#include <stdio.h>
#include <stdlib.h>

int main()
{
    char pita[10] = "Pitagorasz";
    char temp[1];

    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            printf("%c ", pita[j]);
        }
        printf("\n");

        temp[0] = pita[0];

        for (int k = 0; k < 9; k++)
        {
            pita[k] = pita[k+1];
        }
        pita[9] = temp[0];
    }

    return 0;
}
