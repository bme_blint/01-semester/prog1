#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main()
{
    int tomb[10];

    for (int i = 0; i < 10; i += 1)
        tomb[i] = i * 10;

    int i = 0;
    while (true) {
        printf("%d. elem: %d\n", i, tomb[i]);
        i += 1;
    }

    /* az eredeti programban eloszor feltoltunk egy 10 elemu tombot ertekekkel,
    majd utana megprobaljuk kiiratni ennek a tombnek a 20 elemet, azaz tulindexeljuk.
    ez az elso 10 elemig meg tokeletesen fog mukodni, viszont amint eler a 11-20 koze,
    akkor mar nem letezo elemeket akarunk kiiratni, igy un. memoriaszemetet fog kiirni a programunk
    ezert lathatjuk, hogy kulonbozo ertekek jelennek meg a kiirasban

    ha a while ciklus feltetelenek beallitjuk a true-t azzal un. vegtelen ciklust generalunk,
    ugyanis a while ciklus addig fog futni, amig a feltetelvizsgalat eredmenye 1 lesz.
    a true boolen erteke egy, a false erteke 0, igy a ciklusunk vegtelen ciklus lesz

    ahhoz hogy boolen-okat tudjunk hasznalni, include-olni kell az stdbool.h konyvtarat*/

    return 0;
}
