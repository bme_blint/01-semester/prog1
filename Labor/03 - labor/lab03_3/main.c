#include <stdio.h>
#include <stdlib.h>

int main()
{
    char betu;
    while (scanf("%c", &betu) == 1)
        printf("betu='%c', betu=%d\n", betu, betu);

    /*a program beker karaktereket a felhasznalotol, folyamatosan, amig a felhasznalo gepel,
    ki tudja javitani a szoveget, majd ha ENTERT nyom, akkor tortenik meg a kiiratasa a beolvasott karaktereknek,
    illetve azok ASCII kodjanak

    a programbol kilepni a Ctrl+C billentyuvel lehet Windowson, mivel az kuldi el a program szamara az EOF-ot, azaz End Of File jelet,
    aminek a visszateresi erteke specialis, azaz 1 (egy)
    igy a while ciklusnak meglesz a kilepesi feltetele

    a scanf("%c", &betu) == 1  ket szerepe: elso beolvassa a bemenetrol a karaktereket,
    masodik a scanf() fuggvenynek van egy un. visszateresi erteke, ez a beolvasott karakterek szamaval egyezik meg
    */
    return 0;
}
