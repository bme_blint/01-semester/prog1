#include <stdio.h>
#include <stdlib.h>

int main()
{
    int szorzat = 1;
    int n = 8;
    while (n > 1) {
        szorzat *= n;
        n -= 1;
    }
    printf("%d\n", szorzat);

    //a szorzat valtozo erteke 6720, amikor az n valtozo epp 3-ra valt

    //nalam a CodeBlocks 17.12-es verziojaban nem az F4-el, hanem a Shift+F7-el (Step into) lehet bel�pni a lepesenkenti debugolasba
    return 0;
}
